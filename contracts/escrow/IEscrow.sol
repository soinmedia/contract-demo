// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

/**
 * @dev Interface of Escrow contract
 * This contract whill hold the funds for the marketplace, offer, swap until the transaction is completed
*/

interface IEscrow {
  // view
  function balanceOf(address account) external view returns (uint256);
  function allowance(address owner, address spender) external view returns (uint256);

  // for owner
  function setTrusted(address _trusted, bool _isTrusted) external;
  function trusted(address _trusted) external view returns (bool);

  // for market, offer, swap
  function completePayment(address from, address to, uint256 amount) external;

  // for user
  function withdraw(uint256 amount) external;
  function deposit() external payable;
  function depositAndApprove(address spender) external payable;

  function approve(address spender, uint256 amount) external returns (bool);
  function increaseAllowance(address spender, uint256 addedValue) external returns (bool);
  function decreaseAllowance(address spender, uint256 subtractedValue) external returns (bool);
}
