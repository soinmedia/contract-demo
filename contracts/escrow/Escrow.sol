// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol";

import "./IEscrow.sol";

contract Escrow is IEscrow, Initializable, ContextUpgradeable, ReentrancyGuardUpgradeable, OwnableUpgradeable {
    mapping(address => uint256) private _balances;

    mapping(address => mapping(address => uint256)) private _allowances;
    mapping(address => bool) private _trusteds;

    function initialize() public initializer {
      __Ownable_init();
      __ReentrancyGuard_init();
    }

    // == VIEWS =============================================================
    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }

    function allowance(address owner, address spender) public view returns (uint256) {
        return _allowances[owner][spender];
    }

    function trusted(address _trusted) public view override returns (bool) {
      return _trusteds[_trusted];
    }

    // == SETTING ===========================================================
    function setTrusted(address _trusted, bool _isTrusted) external onlyOwner {
        _trusteds[_trusted] = _isTrusted;
    }

    // == USER FUNCTIONS ================================================
    function withdraw(uint256 amount) public {
        address owner = _msgSender();
        require(_balances[owner] >= amount, "Escrow: transfer amount exceeds balance");
        _balances[owner] -= amount;
        payable(owner).transfer(amount);
    }

    function deposit() external payable {
        address owner = _msgSender();
        _balances[owner] += msg.value;
    }

    function depositAndApprove(address spender) external payable {
        address owner = _msgSender();
        _balances[owner] += msg.value;
        _approve(owner, spender, msg.value);
    }

    function approve(address spender, uint256 amount) public returns (bool) {
        address owner = _msgSender();
        _approve(owner, spender, amount);
        return true;
    }

    function increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
        address owner = _msgSender();
        _approve(owner, spender, allowance(owner, spender) + addedValue);
        return true;
    }

    function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
        address owner = _msgSender();
        uint256 currentAllowance = allowance(owner, spender);
        require(currentAllowance >= subtractedValue, "Escrow: decreased allowance below zero");
        unchecked {
            _approve(owner, spender, currentAllowance - subtractedValue);
        }

        return true;
    }

    // == MARKET FUNCTIONS ================================================
    function completePayment(address from, address to, uint256 amount) public nonReentrant onlyTrusted {
        address spender = _msgSender();
        require(_balances[from] >= amount, "Escrow: transfer amount exceeds balance");
        require(_allowances[from][spender] >= amount, "Escrow: transfer amount exceeds allowance");

        _balances[from] -= amount;

        _spendAllowance(from, spender, amount);
        payable(to).transfer(amount);
    }

    // == MODIFIERS ==========================================================
    modifier onlyTrusted() {
        require(_trusteds[_msgSender()], "Escrow: not trusted");
        _;
    }

    // == INTERNAL FUNCTIONS ================================================
    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal {
        require(owner != address(0), "Escrow: approve from the zero address");
        require(spender != address(0), "Escrow: approve to the zero address");

        _allowances[owner][spender] = amount;
    }

    /**
     * @dev Updates `owner` s allowance for `spender` based on spent `amount`.
     *
     * Does not update the allowance amount in case of infinite allowance.
     * Revert if not enough allowance is available.
     */
    function _spendAllowance(
        address owner,
        address spender,
        uint256 amount
    ) internal {
        uint256 currentAllowance = allowance(owner, spender);
        if (currentAllowance != type(uint256).max) {
            require(currentAllowance >= amount, "ERC20: insufficient allowance");
            unchecked {
                _approve(owner, spender, currentAllowance - amount);
            }
        }
    }

    /**
     * @dev This empty reserved space is put in place to allow future versions to add new
     * variables without shifting down storage in the inheritance chain.
     * See https://docs.openzeppelin.com/contracts/4.x/upgradeable#storage_gaps
     */
    uint256[45] private __gap;
}
