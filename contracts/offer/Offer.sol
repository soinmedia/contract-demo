// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/EIP712Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

import "../royalty/IRoyalty.sol";
import "../escrow/IEscrow.sol";
import "../reward/IReward.sol";
import "./IOffer.sol";
import "../market-fee/IMarketFee.sol";

contract Offer is
    Initializable,
    IOffer,
    ReentrancyGuardUpgradeable,
    OwnableUpgradeable,
    EIP712Upgradeable
{
    using SafeMathUpgradeable for uint256;
    using SafeMathUpgradeable for uint16;

    IRoyalty public royaltyRegistry;
    IEscrow public escrow;
    address public marketPayee;
    uint16 public marketPercent;
    IReward public reward;

    IMarketFee public marketFeeRegistry;

    function initialize(address _payee) public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();
        marketPayee = _payee;
        marketPercent = 500;
        __EIP712_init("Offer", "1.0.0");
        __Ownable_init();
        __ReentrancyGuard_init();
        marketPayee = _payee;
        marketPercent = 500;
    }

    // == SETTING ===========================================================
    function setRoyaltyRegistry(address _royaltyRegistry) external onlyOwner {
        royaltyRegistry = IRoyalty(_royaltyRegistry);
    }

    function setFeeRegistry(address _marketFeeRegistry) external onlyOwner {
        marketFeeRegistry = IMarketFee(_marketFeeRegistry);
    }

    function setEscrow(address _escrow) external onlyOwner {
        escrow = IEscrow(_escrow);
    }

    function setReward(address _reward) external onlyOwner {
        reward = IReward(_reward);
    }

    function setMarketPayee(address _payee) external onlyOwner {
        marketPayee = _payee;
    }

    function setMarketPercent(uint16 _percent) external onlyOwner {
        require(_percent <= 10000, "percent must be less than 10000");
        marketPercent = _percent;
    }

    // == FEATURES ==========================================================

    function acceptOffer(
        IOffer.Offer calldata _offer,
        bytes calldata _signature
    ) external nonReentrant escrowSet {
        address bidder = ECDSAUpgradeable.recover(
            _hashOffer(_offer),
            _signature
        );

        require(bidder == _offer.bidder, "invalid signature");
        require(
            IERC721(_offer.tokenAddress).ownerOf(_offer.tokenId) == msg.sender,
            "not own nft"
        );

        // transfer nft to bidder
        IERC721(_offer.tokenAddress).transferFrom(
            msg.sender,
            bidder,
            _offer.tokenId
        );

        _payout(_offer.tokenAddress, _offer.price, _offer.bidder, msg.sender);
        emit OfferAccepted(
            _offer.tokenAddress,
            _offer.tokenId,
            _offer.price,
            _offer.bidder,
            msg.sender
        );
    }

    function acceptCollectionOffer(
        IOffer.Offer calldata _offer,
        bytes calldata _signature
    ) external nonReentrant escrowSet {
        address bidder = ECDSAUpgradeable.recover(
            _hashCollectionOffer(_offer),
            _signature
        );

        require(bidder == _offer.bidder, "invalid signature");
        require(
            IERC721(_offer.tokenAddress).ownerOf(_offer.tokenId) == msg.sender,
            "not own nft"
        );

        // transfer nft to bidder
        IERC721(_offer.tokenAddress).transferFrom(
            msg.sender,
            bidder,
            _offer.tokenId
        );

        _payout(_offer.tokenAddress, _offer.price, _offer.bidder, msg.sender);
        emit OfferAccepted(
            _offer.tokenAddress,
            _offer.tokenId,
            _offer.price,
            _offer.bidder,
            msg.sender
        );
    }

    function acceptCounterOffer(
        IOffer.CounterOffer calldata _counterOffer,
        bytes calldata _signature
    ) external nonReentrant escrowSet {
        address seller = ECDSAUpgradeable.recover(
            _hashCounterOffer(_counterOffer),
            _signature
        );

        require(seller == _counterOffer.seller, "invalid signature");
        require(
            IERC721(_counterOffer.tokenAddress).ownerOf(
                _counterOffer.tokenId
            ) == _counterOffer.seller,
            "seller not own nft"
        );

        // transfer nft to bidder
        IERC721(_counterOffer.tokenAddress).transferFrom(
            _counterOffer.seller,
            msg.sender,
            _counterOffer.tokenId
        );

        _payout(
            _counterOffer.tokenAddress,
            _counterOffer.price,
            msg.sender,
            _counterOffer.seller
        );

        emit CounterOfferAccepted(
            _counterOffer.tokenAddress,
            _counterOffer.tokenId,
            _counterOffer.price,
            msg.sender,
            _counterOffer.seller
        );
    }

    function acceptPackageOffer(
        IOffer.PackageOffer calldata _packageOffer,
        bytes calldata _signature
    ) external nonReentrant escrowSet {
        address bidder = ECDSAUpgradeable.recover(
            _hashPackageOffer(_packageOffer),
            _signature
        );
        require(bidder == _packageOffer.bidder, "invalid signature");

        for (uint256 i = 0; i < _packageOffer.tokenIds.length; i++) {
            IERC721(_packageOffer.tokenAddress).transferFrom(
                msg.sender,
                bidder,
                _packageOffer.tokenIds[i]
            );
        }

        _payout(
            _packageOffer.tokenAddress,
            _packageOffer.price,
            _packageOffer.bidder,
            msg.sender
        );

        emit PackageOfferAccepted(
            _packageOffer.tokenAddress,
            _packageOffer.price,
            _packageOffer.bidder,
            msg.sender,
            _packageOffer.id
        );
    }

    // == MODIFIER ==========================================================
    modifier escrowSet() {
        require(address(escrow) != address(0), "escrow is not set");
        _;
    }

    // == HELPERS ===========================================================

    function _payout(
        address _tokenAddress,
        uint256 _price,
        address _bidder,
        address _owner
    ) internal {
        uint256 _marketPercent = marketPercent;

        if (address(marketFeeRegistry) != address(0)) {
            _marketPercent = marketFeeRegistry.getFee(_tokenAddress);
        } else {
            _marketPercent = marketPercent;
        }

        // extract market fee
        uint256 marketFee = _price.mul(_marketPercent).div(10000);
        escrow.completePayment(_bidder, marketPayee, marketFee);

        uint256 balance = _price.sub(_price.mul(_marketPercent).div(10000));

        // pay for collection payee
        if (address(royaltyRegistry) != address(0)) {
            address collectionPayee = royaltyRegistry.getCollectionPayee(
                _tokenAddress
            );
            uint16 collectionRoyalty = royaltyRegistry.getCollectionRoyalty(
                _tokenAddress
            );
            if (collectionPayee != address(0) && collectionRoyalty > 0) {
                uint256 collectionRevenue = _price.mul(collectionRoyalty).div(
                    10000
                );
                escrow.completePayment(
                    _bidder,
                    collectionPayee,
                    collectionRevenue
                );
                balance = balance.sub(collectionRevenue);
            }
        }

        // pay for _owner
        escrow.completePayment(_bidder, _owner, balance);
        if (address(reward) != address(0)) {
            reward.reward(_owner, _bidder, _tokenAddress, _price);
        }
    }

    function _hashOffer(IOffer.Offer calldata _offer)
        private
        view
        returns (bytes32)
    {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "OfferParams(address tokenAddress,uint256 tokenId,uint256 price,address bidder)"
                        ),
                        _offer.tokenAddress,
                        _offer.tokenId,
                        _offer.price,
                        _offer.bidder
                    )
                )
            );
    }

    function _hashCollectionOffer(IOffer.Offer calldata _offer)
        private
        view
        returns (bytes32)
    {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "CollectionOfferParams(address tokenAddress,uint256 price,address bidder)"
                        ),
                        _offer.tokenAddress,
                        _offer.price,
                        _offer.bidder
                    )
                )
            );
    }

    function _hashCounterOffer(IOffer.CounterOffer calldata _counterOffer)
        private
        view
        returns (bytes32)
    {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "CounterOfferParams(address tokenAddress,uint256 tokenId,uint256 price,address seller)"
                        ),
                        _counterOffer.tokenAddress,
                        _counterOffer.tokenId,
                        _counterOffer.price,
                        _counterOffer.seller
                    )
                )
            );
    }

    function _hashPackageOffer(IOffer.PackageOffer calldata _packageOffer)
        private
        view
        returns (bytes32)
    {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "PackageOfferParams(address tokenAddress,uint256[] tokenIds,uint256 price,address bidder,uint256 id)"
                        ),
                        _packageOffer.tokenAddress,
                        keccak256(abi.encodePacked(_packageOffer.tokenIds)),
                        _packageOffer.price,
                        _packageOffer.bidder,
                        _packageOffer.id
                    )
                )
            );
    }
}
