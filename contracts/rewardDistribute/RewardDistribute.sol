// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract RewardDistribute is Ownable {
    IERC721Enumerable public corgi;
    address public corgiPayee;
    uint256 public fee = 0.1 ether;
    uint256 public corgiFee = 0.05 ether;

    IERC721Enumerable public collection;

    IERC20 public rewardToken;
    uint256 public currentRoyalties;
    uint256 public totalRoyalties;

    mapping(uint256 => uint256) public claimedRoyalties;

    struct TokenInfo {
        uint256 id;
        uint256 royalties;
    }

    // Events
    event RoyaltiesAdded(uint256 amount);
    event RoyaltiesClaimed(address indexed sender, uint256 amount);

    // Errors
    error AddingNoRewards();

    // Init
    constructor(
        address _collectionAddress,
        address _rewardAddress,
        address _owner,
        address _corgiAddress,
        address _corgiPayee,
        uint256 _deployFee
    ) payable {
        require(msg.value == _deployFee, "Deployment fee is not correct");
        collection = IERC721Enumerable(_collectionAddress);
        rewardToken = IERC20(_rewardAddress);
        corgi = IERC721Enumerable(_corgiAddress);
        corgiPayee = _corgiPayee;
        transferOwnership(_owner);

        payable(_corgiPayee).transfer(msg.value);
    }

    // Royalties
    function createDistribute(uint256 amount) external onlyOwner {
        if (amount == 0) revert AddingNoRewards();
        require(collection.totalSupply() > 0, "No tokens to distribute");
        uint256 distribute = (amount * 97) / 100;

        totalRoyalties = totalRoyalties + distribute;
        currentRoyalties += distribute / collection.totalSupply();

        // 97% to distribute
        rewardToken.transferFrom(msg.sender, address(this), distribute);
        // 3% to corgi
        rewardToken.transferFrom(msg.sender, corgiPayee, (amount * 3) / 100);

        emit RoyaltiesAdded(distribute);
    }

    // Getters
    function getClaimFee(address sender) public view returns (uint256) {
        if (corgi.balanceOf(sender) > 0) {
            return corgiFee;
        }
        return fee;
    }

    function getRewardsToken(uint256 id) public view returns (uint256 rewards) {
        rewards += currentRoyalties - claimedRoyalties[id];
    }

    function getRoyalties(address sender) external view returns (uint256) {
        uint256 balance = 0;

        uint256 count = collection.balanceOf(sender);
        for (uint256 i = 0; i < count; i++) {
            uint256 tokenId = collection.tokenOfOwnerByIndex(sender, i);
            balance += getRewardsToken(tokenId);
        }
        return balance;
    }

    function getRoyaltiesDetails(address sender)
        external
        view
        returns (TokenInfo[] memory)
    {
        uint256 count = collection.balanceOf(sender);
        TokenInfo[] memory tokens = new TokenInfo[](count);

        for (uint256 i = 0; i < count; i++) {
            uint256 tokenId = collection.tokenOfOwnerByIndex(sender, i);
            uint256 royalties = getRewardsToken(tokenId);
            tokens[i].id = tokenId;
            tokens[i].royalties = royalties;
        }

        return tokens;
    }

    // Claim
    function claimAllRoyalties() external payable {
        require(msg.value >= getClaimFee(msg.sender), "Not enough fee");
        uint256 rewards = 0;

        uint256 count = collection.balanceOf(msg.sender);
        require(count > 0, "No tokens to claim");
        require(count < 100, "Too many tokens to claim");

        for (uint256 i = 0; i < count; i++) {
            uint256 tokenId = collection.tokenOfOwnerByIndex(msg.sender, i);
            unchecked {
                rewards += getRewardsToken(tokenId);
                claimedRoyalties[tokenId] = currentRoyalties;
                ++i;
            }
        }

        rewardToken.transfer(_msgSender(), rewards);
        // send fee to corgi payee
        payable(corgiPayee).transfer(msg.value);

        emit RoyaltiesClaimed(_msgSender(), rewards);
    }

    function claimRoyalties(uint256[] memory tokensToClaim) external payable {
        require(msg.value >= getClaimFee(msg.sender), "Not enough fee");
        uint256 rewards = 0;

        for (uint256 i = 0; i < tokensToClaim.length; ) {
            unchecked {
                uint256 tokenId = tokensToClaim[i];
                if (collection.ownerOf(tokenId) == _msgSender()) {
                    rewards += (getRewardsToken(tokenId));
                    claimedRoyalties[tokenId] = currentRoyalties;
                }
                ++i;
            }
        }

        rewardToken.transfer(_msgSender(), rewards);
        // send fee to corgi payee
        payable(corgiPayee).transfer(msg.value);

        emit RoyaltiesClaimed(_msgSender(), rewards);
    }

    function withdrawAll() external onlyOwner {
        rewardToken.transfer(
            _msgSender(),
            rewardToken.balanceOf(address(this))
        );

        payable(corgiPayee).transfer(address(this).balance);
    }
}
