pragma solidity ^0.8.9;
// SPDX-License-Identifier: MIT

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/cryptography/SignatureChecker.sol";
import "@openzeppelin/contracts/utils/cryptography/EIP712.sol";

contract Launchpad is ERC721Enumerable, Ownable, EIP712 {
    enum TokenType {
        NATIVE,
        ERC20,
        ERC721
    }

    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    string public baseURI;
    uint256 public limit;
    uint256 public userLimit;

    mapping(address => uint256) public minted;

    address _signer;

    // payees detail
    address[] public payees;
    uint256[] public payeePercents; // 2500 = 25%, total 100%

    // corgi fee
    address public corgiPayee;
    uint256 public corgiNativePercent = 1000; // 10%
    uint256 public corgiERC20Percent = 1500; // 15%
    uint256 public corgiERC721Fee = 3 ether; // TODO: update this for polygon

    string public maskLink;

    constructor(
        string memory _name,
        string memory _symbol,
        string memory _baseURI,
        uint256 _limit,
        uint256 _userLimit,
        address[] memory _payees,
        uint256[] memory _payeePercents,
        address signer_,
        address _corgiPayee
    ) ERC721(_name, _symbol) EIP712("Launchpad", "1.0.0") payable {
        baseURI = _baseURI;
        _signer = signer_;
        corgiPayee = _corgiPayee;

        setLimit(_limit, _userLimit);
        setPayees(_payees, _payeePercents);
        payable(corgiPayee).transfer(msg.value);
    }

    // ======================== SETTING =====================================
    function setBaseURI(string memory _baseURI) public onlyOwner {
        baseURI = _baseURI;
    }

    function setLimit(uint256 _limit, uint256 _userLimit) public onlyOwner {
        limit = _limit;
        userLimit = _userLimit;
    }

    function setPayees(
        address[] memory _payees,
        uint256[] memory _payeePercents
    ) public onlyOwner {
        uint256 totalPercent = 0;
        for (uint256 i = 0; i < _payeePercents.length; i++) {
            totalPercent += _payeePercents[i];
        }
        require(totalPercent == 10000, "r0");
        payees = _payees;
        payeePercents = _payeePercents;
    }

    function maskUri(string memory _maskLink) public onlyOwner {
      maskLink = _maskLink;
    }

    // ======================== GETTERS =====================================
    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721)
        returns (string memory)
    {
        require(_exists(tokenId), "nonexistent token");
        if (bytes(maskLink).length > 0) {
          return maskLink;
        }

        return
            string(
                abi.encodePacked(baseURI, Strings.toString(tokenId), ".json")
            );
    }

    // ======================== MINT =====================================
    function _mint(
        address _from,
        address _to,
        uint256 _amount
    ) internal {
        require(_tokenIds.current() + _amount <= limit, "r1");
        // check if _from is not owner
        if (_from != owner()) {
          require(minted[_from] + _amount <= userLimit, "r2");
        }

        for (uint256 i = 0; i < _amount; i++) {
            _tokenIds.increment();
            _safeMint(_to, _tokenIds.current());
        }
        minted[_from] += _amount;
    }

    // WARN: ERC721 must be first
    function mint(
        address _to,
        uint256 _quantity,
        TokenType[] memory _types,
        address[] calldata _tokens,
        uint256[] calldata _tokenAmounts,
        uint16[] memory _nftIdsPay,
        bytes calldata _signature
    ) public payable {
        require(
            SignatureChecker.isValidSignatureNow(
                _signer,
                _hash(msg.sender, _quantity, _tokens, _tokenAmounts),
                _signature
            ),
            "r3"
        );

        _payment(
            msg.sender,
            _types[0],
            _tokens[0],
            _tokenAmounts[0],
            _nftIdsPay,
            0,
            _quantity
        );

        if (_tokens.length > 1) {
            _payment(
                msg.sender,
                _types[1],
                _tokens[1],
                _tokenAmounts[1],
                _nftIdsPay,
                _tokenAmounts[0],
                _quantity
            );
        }

        _mint(msg.sender, _to, _quantity);
    }

    function _payment(
        address _from,
        TokenType _type,
        address _token,
        uint256 _amount,
        uint16[] memory _nftIds,
        uint256 _index,
        uint256 _quantity
    ) internal {
        if (_type == TokenType.NATIVE) {
            _payNative(_amount);
        } else if (_type == TokenType.ERC20) {
            _payERC20(_token, _from, _amount);
        } else if (_type == TokenType.ERC721) {
            (bool success, ) = corgiPayee.call{
                value: _quantity * corgiERC721Fee
            }("");
            require(success, "r7");
            _payERC721(_token, _from, _amount, _nftIds, _index);
        } else {
            revert("Invalid token type");
        }
    }

    function _payNative(uint256 _amount) internal {
        uint256 corgiFee = (_amount * corgiNativePercent) / 10000;
        uint256 payeeAmount = _amount - corgiFee;

        // pay corgi, use call to prevent revert
        (bool success, ) = corgiPayee.call{value: corgiFee}("");
        require(success, "r5");

        // pay payees
        for (uint256 i = 0; i < payees.length; i++) {
            uint256 payeeFee = (payeeAmount * payeePercents[i]) / 10000;
            (success, ) = payees[i].call{value: payeeFee}("");
            require(success, "r6");
        }
    }

    function _payERC20(
        address _token,
        address _from,
        uint256 _amount
    ) internal {
        uint256 corgiFee = (_amount * corgiERC20Percent) / 10000;
        uint256 payeeAmount = _amount - corgiFee;

        // pay corgi
        IERC20(_token).transferFrom(_from, corgiPayee, corgiFee);

        // pay payees
        for (uint256 i = 0; i < payees.length; i++) {
            uint256 payeeFee = (payeeAmount * payeePercents[i]) / 10000;
            IERC20(_token).transferFrom(_from, payees[i], payeeFee);
        }
    }

    function _payERC721(
        address _token,
        address _from,
        uint256 _amount,
        uint16[] memory _nftIds,
        uint256 _index
    ) internal {
        for (uint256 i = _index; i < _index + _amount; i++) {
            IERC721(_token).transferFrom(_from, payees[0], _nftIds[i]);
        }
    }

    // ======================== SUPPORT =====================================
    function _hash(
        address _from,
        uint256 _quantity,
        address[] calldata _tokens,
        uint256[] calldata _tokenAmounts
    ) private view returns (bytes32) {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "MintParams(address from,uint256 quantity,address[] tokens,uint256[] tokenAmounts,uint256 value)"
                        ),
                        _from,
                        _quantity,
                        keccak256(abi.encodePacked(_tokens)),
                        keccak256(abi.encodePacked(_tokenAmounts)),
                        msg.value
                    )
                )
            );
    }
}
