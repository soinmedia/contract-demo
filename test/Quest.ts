import { expect } from "chai";
import { ethers, upgrades } from "hardhat";
import { createQuestSignature } from "../helpers/quest";

describe("Quest", function () {
  async function deploy() {
    const [owner, user1, user2, user3, signer] = await ethers.getSigners();
    const Quest = await ethers.getContractFactory("Quest");

    const ERC20 = await ethers.getContractFactory("ERC20Mock");
    const coin = await ERC20.deploy("Coin", "COIN");
    await coin.deployed();

    const quest = await upgrades.deployProxy(Quest, [signer.address]);

    const chainId = 31337;

    return { quest, owner, user1, user2, user3, signer, coin, chainId };
  }

  describe("Deployment", function () {
    it("Should set the right owner", async function () {
      const { quest, owner } = await deploy();
      expect(await quest.owner()).to.equal(owner.address);
    });

    it("Should set the right signer", async function () {
      const { quest, signer } = await deploy();
      expect(await quest.signerAddress()).to.equal(
        signer.address
      );
    });
  });

  describe("Setting", function () {
    it("Should set signer", async function () {
      const { quest, owner, user1 } = await deploy();
      await quest.connect(owner).setSigner(user1.address);
      expect(await quest.signerAddress()).to.equal(user1.address);
    });

    it("Should setting only by owner", async function () {
      const { quest, user1 } = await deploy();
      await expect(
        quest.connect(user1).setSigner(user1.address)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });

  describe("Claim", function () {
    it("Should throw if wrong signature", async function () {
      const { quest, user1, user2, coin, signer, chainId } = await deploy();
      const params = {
        questId: 1,
        rewardCoin: coin.address,
        rewardAmount: ethers.utils.parseEther("1"),
        user: user1.address,
      };

      const signature = createQuestSignature(
        signer,
        quest.address,
        params,
        chainId
      )

      await expect(
        quest.connect(user2).claim(
          params.questId,
          params.rewardCoin,
          params.rewardAmount,
          signature
        )
      ).to.be.revertedWith("Quest: invalid signature");
    });

    it("Should claim", async function () {
      const { quest, user1, coin, signer, chainId } = await deploy();
      
      // provide coin to quest
      await coin.transfer(quest.address, ethers.utils.parseEther("1"));

      const params = {
        questId: 1,
        rewardCoin: coin.address,
        rewardAmount: ethers.utils.parseEther("1"),
        user: user1.address,
      };

      const signature = await createQuestSignature(
        signer,
        quest.address,
        params,
        chainId
      )

      await expect(
        quest.connect(user1).claim(
          params.questId,
          params.rewardCoin,
          params.rewardAmount,
          signature
        )
      ).to.emit(quest, "Claimed");

      expect(await coin.balanceOf(user1.address)).to.equal(
        params.rewardAmount
      );
    });

    it("Should claim only once", async function () {
      const { quest, user1, coin, signer, chainId } = await deploy();
      
      // provide coin to quest
      await coin.transfer(quest.address, ethers.utils.parseEther("1"));

      const params = {
        questId: 1,
        rewardCoin: coin.address,
        rewardAmount: ethers.utils.parseEther("1"),
        user: user1.address,
      };

      const signature = await createQuestSignature(
        signer,
        quest.address,
        params,
        chainId
      )

      await quest.connect(user1).claim(
        params.questId,
        params.rewardCoin,
        params.rewardAmount,
        signature
      );

      await expect(
        quest.connect(user1).claim(
          params.questId,
          params.rewardCoin,
          params.rewardAmount,
          signature
        )
      ).to.be.revertedWith("Quest: already claimed");
    });
  });
});
