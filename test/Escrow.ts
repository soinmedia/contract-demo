import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers, upgrades } from "hardhat";

describe("Escrow", function () {
  async function deploy() {
    const [owner, user1, user2, user3, spender] = await ethers.getSigners();
    const Escrow = await ethers.getContractFactory("Escrow");
    const escrow = await upgrades.deployProxy(Escrow, []);
    await escrow.deployed();
    await escrow.setTrusted(spender.address, true);

    return { owner, user1, user2, user3, spender, escrow };
  }

  describe("Deployment", function () {
    it("Should set the right owner", async function () {
      const { owner, escrow } = await loadFixture(deploy);
      expect(await escrow.owner()).to.equal(owner.address);
    });
  });

  describe("Sending", function () {
    it("Should set trusted", async function () {
      const { escrow, spender } = await loadFixture(deploy);
      await escrow.setTrusted(spender.address, true);
      expect(await escrow.trusted(spender.address)).to.equal(true);
    });

    it("Should change owner", async function () {
      const { escrow, user1 } = await loadFixture(deploy);
      await escrow.transferOwnership(user1.address);
      expect(await escrow.owner()).to.equal(user1.address);
    })
  });

  describe("completePayment", function () {
    it("Should revert if not trusted", async function () {
      const { user1, escrow, user2 } = await loadFixture(deploy);
      await expect(
        escrow
          .connect(user1)
          .completePayment(
            user1.address,
            user2.address,
            ethers.utils.parseEther("1")
          )
      ).to.be.revertedWith("Escrow: not trusted");
    });

    it("Should revert if not enough balance", async function () {
      const { user1, escrow, spender, user2 } = await loadFixture(deploy);
      await expect(
        escrow
          .connect(spender)
          .completePayment(
            user1.address,
            user2.address,
            ethers.utils.parseEther("1")
          )
      ).to.be.revertedWith("Escrow: transfer amount exceeds balance");

      // deposit and approve 1 eth
      await escrow.connect(user1).depositAndApprove(spender.address, {
        value: ethers.utils.parseEther("1"),
      });

      // transfer 2 ether
      await expect(
        escrow
          .connect(spender)
          .completePayment(
            user1.address,
            user2.address,
            ethers.utils.parseEther("2")
          )
      ).to.be.revertedWith("Escrow: transfer amount exceeds balance");
    });

    it("Should revert if not enough allowance", async function () {
      const { user1, escrow, spender, user2 } = await loadFixture(deploy);
      // top up 1 ether
      await escrow.connect(user1).depositAndApprove(spender.address, {
        value: ethers.utils.parseEther("1"),
      });

      await escrow
        .connect(user1)
        .decreaseAllowance(spender.address, ethers.utils.parseEther("0.3"));

      // transfer 0.9 > 0.7
      await expect(
        escrow
          .connect(spender)
          .completePayment(
            user1.address,
            user2.address,
            ethers.utils.parseEther("0.9")
          )
      ).to.be.revertedWith("Escrow: transfer amount exceeds allowance");
    });

    it("Should completePayment", async function () {
      const { user1, escrow, spender, user2 } = await loadFixture(deploy);

      await escrow.connect(user1).depositAndApprove(spender.address, {
        value: ethers.utils.parseEther("1"),
      });

      const completePayment = escrow
        .connect(spender)
        .completePayment(
          user1.address,
          user2.address,
          ethers.utils.parseEther("0.7")
        );
      await completePayment;

      // Escrow state
      expect(await escrow.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.3")
      );
      expect(await escrow.allowance(user1.address, spender.address)).to.equal(
        ethers.utils.parseEther("0.3")
      );

      // Wallet balance
      await expect(completePayment).to.changeEtherBalance(
        escrow,
        ethers.utils.parseEther("-0.7")
      );

      await expect(completePayment).to.changeEtherBalance(
        user2,
        ethers.utils.parseEther("0.7")
      );
    });
  });

  describe("withdraw", function () {
    it("Should revert if not enough balance", async function () {
      const { escrow, spender } = await loadFixture(deploy);
      await expect(
        escrow.connect(spender).withdraw(ethers.utils.parseEther("1"))
      ).to.be.revertedWith("Escrow: transfer amount exceeds balance");
    });

    it("Should transfer the amount", async function () {
      const { escrow, spender, user1 } = await loadFixture(deploy);

      await escrow.connect(user1).depositAndApprove(spender.address, {
        value: ethers.utils.parseEther("1"),
      });

      const withdraw = escrow
        .connect(user1)
        .withdraw(ethers.utils.parseEther("0.7"));
      await withdraw;

      // Escrow state
      expect(await escrow.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.3")
      );

      // Wallet balance
      await expect(withdraw).to.changeEtherBalance(
        escrow,
        ethers.utils.parseEther("-0.7")
      );

      await expect(withdraw).to.changeEtherBalance(
        user1,
        ethers.utils.parseEther("0.7")
      );
    });
  });

  describe("deposit", function () {
    it("Should deposit", async function () {
      const { escrow, user1 } = await loadFixture(deploy);
      const deposit = escrow.connect(user1).deposit({
        value: ethers.utils.parseEther("1"),
      });
      await deposit;

      // Escrow state
      expect(await escrow.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("1")
      );

      // Wallet balance
      await expect(deposit).to.changeEtherBalance(
        escrow,
        ethers.utils.parseEther("1")
      );

      await expect(deposit).to.changeEtherBalance(
        user1,
        ethers.utils.parseEther("-1")
      );
    })
  })

  describe("approve", function () {
    it("Should approve the amount", async function () {
      const { escrow, user1, spender } = await loadFixture(deploy);
      await escrow
        .connect(user1)
        .approve(spender.address, ethers.utils.parseEther("1"));
      expect(await escrow.allowance(user1.address, spender.address)).to.equal(
        ethers.utils.parseEther("1")
      );
    });

    it("Should increase the allowance", async function () {
      const { user1, escrow, spender } = await loadFixture(deploy);
      await escrow
        .connect(user1)
        .increaseAllowance(spender.address, ethers.utils.parseEther("0.7"));
      expect(await escrow.allowance(user1.address, spender.address)).to.equal(
        ethers.utils.parseEther("0.7")
      );
    });

    it("Should decrease the allowance", async function () {
      const { user1, escrow, spender } = await loadFixture(deploy);
      await escrow.connect(user1).depositAndApprove(spender.address, {
        value: ethers.utils.parseEther("1"),
      });

      await escrow
        .connect(user1)
        .decreaseAllowance(spender.address, ethers.utils.parseEther("0.1"));
      expect(await escrow.allowance(user1.address, spender.address)).to.equal(
        ethers.utils.parseEther("0.9")
      );
    });

    it("Should throw if decrease more than allowance", async function () {
      const { user1, escrow, spender } = await loadFixture(deploy);
      await expect(
        escrow
          .connect(user1)
          .decreaseAllowance(spender.address, ethers.utils.parseEther("0.7"))
      ).to.be.revertedWith("Escrow: decreased allowance below zero");
    });
  });
});
