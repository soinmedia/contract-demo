import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers, upgrades } from "hardhat";

describe("Reward", function () {
  async function deploy() {
    const [owner, user1, user2, user3, spender, collection] =
      await ethers.getSigners();
    const Reward = await ethers.getContractFactory("Reward");
    const reward = await upgrades.deployProxy(Reward, []);
    await reward.deployed();

    const ERC20 = await ethers.getContractFactory("ERC20Mock");
    const erc20 = await ERC20.deploy("reward", "rw");
    await erc20.deployed();

    await reward.setTrusted(spender.address, true);

    // set tiers
    await reward.setTier(1, {
      seller: ethers.utils.parseEther("0.1"), // 0.1 coin per 1 CRO
      buyer: ethers.utils.parseEther("0.2"), // 0.2 coin per 1 CRO
      collectionOwner: ethers.utils.parseEther("0.3"), // 0.3 coin per 1 CRO
    });

    await reward.setTier(2, {
      seller: ethers.utils.parseEther("0.2"),
      buyer: ethers.utils.parseEther("0.3"),
      collectionOwner: ethers.utils.parseEther("0.4"),
    });

    return { owner, user1, user2, user3, spender, reward, collection, erc20 };
  }

  describe("Deployment", function () {
    it("Should set the right owner", async function () {
      const { owner, reward } = await loadFixture(deploy);
      expect(await reward.owner()).to.equal(owner.address);
    });
  });

  describe("Setting", function () {
    it("Should set trusted", async function () {
      const { reward, spender } = await loadFixture(deploy);
      await reward.setTrusted(spender.address, true);
      expect(await reward.trusted(spender.address)).to.equal(true);
    });

    it("Should change owner", async function () {
      const { reward, user1 } = await loadFixture(deploy);
      await reward.transferOwnership(user1.address);
      expect(await reward.owner()).to.equal(user1.address);
    });
  });

  describe("reward", function () {
    it("Should revert if not trusted", async function () {
      const { user1, reward, user2, user3 } = await loadFixture(deploy);
      await expect(
        reward.connect(user1).reward(
          user1.address, // seller
          user2.address, // buyer
          user3.address, // collection address
          ethers.utils.parseEther("1")
        )
      ).to.be.revertedWith("Reward: not trusted");
    });

    it("Should not reward if tier 0", async function () {
      const { user1, reward, user2, user3, spender, collection } =
        await loadFixture(deploy);
      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("1")
      );
      // default tier is 0
      expect(await reward.balanceOf(user1.address)).to.equal(0);
      expect(await reward.balanceOf(user2.address)).to.equal(0);
    });

    it("Should reward when collectionOwner not set", async function () {
      const { user1, reward, user2, spender, collection } = await loadFixture(
        deploy
      );
      await reward.setCollectionToTier(collection.address, 1);

      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("1")
      );
      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.1")
      );
      expect(await reward.balanceOf(user2.address)).to.equal(
        ethers.utils.parseEther("0.2")
      );

      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("5")
      );
      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.6")
      );
      expect(await reward.balanceOf(user2.address)).to.equal(
        ethers.utils.parseEther("1.2")
      );
    });

    it("Should reward when collectionOwner set", async function () {
      const { user1, reward, user2, user3, spender, collection } =
        await loadFixture(deploy);
      await reward.setCollectionToTier(collection.address, 1);
      await reward.setCollectionToPayee(collection.address, user3.address);

      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("1")
      );
      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.1")
      );
      expect(await reward.balanceOf(user2.address)).to.equal(
        ethers.utils.parseEther("0.2")
      );
      expect(await reward.balanceOf(user3.address)).to.equal(
        ethers.utils.parseEther("0.3")
      );

      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("5")
      );
      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.6")
      );
      expect(await reward.balanceOf(user2.address)).to.equal(
        ethers.utils.parseEther("1.2")
      );
      expect(await reward.balanceOf(user3.address)).to.equal(
        ethers.utils.parseEther("1.8")
      );
    });

    it("Should emit event", async function () {
      const { user1, reward, user2, user3, spender, collection } =
        await loadFixture(deploy);
      await reward.setCollectionToTier(collection.address, 1);
      await reward.setCollectionToPayee(collection.address, user3.address);

      await expect(
        reward.connect(spender).reward(
          user1.address, // seller
          user2.address, // buyer
          collection.address, // collection address
          ethers.utils.parseEther("1")
        )
      ).to.emit(reward, "Rewarded");
    });
  });

  describe("claim", function () {
    it("Should revert if balance is 0", async function () {
      const { user1, reward } = await loadFixture(deploy);
      await expect(reward.connect(user1).claim()).to.be.revertedWith(
        "Reward: no reward to claim"
      );
    });

    it("Should revert if reward token not set", async function () {
      const { user1, reward, user2, collection, spender } = await loadFixture(deploy);

      await reward.setCollectionToTier(collection.address, 1);
      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("1")
      );

      await expect(reward.connect(user1).claim()).to.be.revertedWith(
        "Reward: reward token not set"
      );
    });

    it("Should revert if contract balance not enough", async function () {
      const { user1, reward, user2, collection, spender, erc20 } = await loadFixture(deploy);

      await reward.setCollectionToTier(collection.address, 1);
      await reward.setRewardToken(erc20.address);
      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("1")
      );

      await expect(reward.connect(user1).claim()).to.be.revertedWith(
        "Reward: reward token not enough"
      );
    })

    it("Should transfer the amount", async function () {
      const { user1, reward, user2, user3, spender, collection, erc20 } =
        await loadFixture(deploy);
      await reward.setCollectionToTier(collection.address, 1);
      await reward.setCollectionToPayee(collection.address, user3.address);
      await reward.setRewardToken(erc20.address);
      await erc20.transfer(reward.address, ethers.utils.parseEther("10"));

      await reward.connect(spender).reward(
        user1.address, // seller
        user2.address, // buyer
        collection.address, // collection address
        ethers.utils.parseEther("1")
      );

      await reward.connect(user1).claim();
      expect(await erc20.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.1")
      );
      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0")
      );
      expect(await erc20.balanceOf(reward.address)).to.equal(
        ethers.utils.parseEther("9.9")
      );
    });
  });
});
