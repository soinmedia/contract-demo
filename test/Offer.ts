import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers, upgrades } from "hardhat";
import { caeateOfferSignature, CollectionOfferParams, CollectionPackageOfferParams, CounterOfferParams, createCollectionOfferSignature, createCollectionPackageOfferSignature, createOfferCounterSignature, OfferParams } from "../helpers/offer";

describe("Offer", function () {
  async function deploy() {
    const [owner, user1, user2, user3, payee] = await ethers.getSigners();
    const Offer = await ethers.getContractFactory("Offer");
    const Nft = await ethers.getContractFactory("NFTMock");
    const Escrow = await ethers.getContractFactory("Escrow");

    const offer = await upgrades.deployProxy(Offer, [payee.address]);
    const nft = await Nft.deploy("nft", "nft");
    const escrow = await upgrades.deployProxy(Escrow, []);
    offer.setEscrow(escrow.address);
    await escrow.setTrusted(offer.address, true);

    const chainId = 31337;

    return { offer, nft, owner, user1, user2, user3, payee, escrow, chainId };
  }

  describe("Deployment", function () {
    it("Should set the right market payee", async function () {
      const { offer, payee } = await deploy();
      expect(await offer.marketPayee()).to.equal(payee.address);
      expect(await offer.marketPercent()).to.equal(500);
    });

    it("Should set the right owner", async function () {
      const { offer, owner } = await deploy();
      expect(await offer.owner()).to.equal(owner.address);
    });

    it("Should have default royalty registry", async function () {
      const { offer } = await deploy();
      expect(await offer.royaltyRegistry()).to.equal(
        ethers.constants.AddressZero
      );
    });
  });

  describe("Setting", function () {
    it("Should set market payee", async function () {
      const { offer, owner, user1 } = await deploy();
      await offer.connect(owner).setMarketPayee(user1.address);
      expect(await offer.marketPayee()).to.equal(user1.address);
    });

    it("Should set market percent", async function () {
      const { offer, owner } = await deploy();
      await offer.connect(owner).setMarketPercent(10);
      expect(await offer.marketPercent()).to.equal(10);
    });

    it("Should set royalty registry", async function () {
      const { offer, owner, user1 } = await deploy();
      await offer.connect(owner).setRoyaltyRegistry(user1.address);
      expect(await offer.royaltyRegistry()).to.equal(user1.address);
    });

    it("Should set escrow", async function () {
      const { offer, owner, user1 } = await deploy();
      await offer.connect(owner).setEscrow(user1.address);
      expect(await offer.escrow()).to.equal(user1.address);
    });
    it("Should setting only by owner", async function () {
      const { offer, user1 } = await deploy();
      await expect(
        offer.connect(user1).setMarketPayee(user1.address)
      ).to.be.revertedWith("Ownable: caller is not the owner");

      await expect(
        offer.connect(user1).setMarketPercent(10)
      ).to.be.revertedWith("Ownable: caller is not the owner");

      await expect(
        offer.connect(user1).setRoyaltyRegistry(user1.address)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });

  describe("Accept offer", function () {
    describe("Validations", function () {
      it("Should revert if not NFT owner", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.mint(user2.address, 2);

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptOffer(offerParams, signature)
        ).to.be.revertedWith("not own nft");
      });

      it("Should revert if invalid signature", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          {
            ...offerParams,
            price: ethers.utils.parseEther("2"),
          },
          chainId
        );

        await expect(
          offer.connect(user1).acceptOffer(offerParams, signature)
        ).to.be.revertedWith("invalid signature");
      });

      it("Should revert if not approve NFT", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user1).acceptOffer(offerParams, signature)
        ).to.be.revertedWith("ERC721: caller is not token owner or approved");
      });

      it("Should revert if not enough escrow balance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        await escrow.connect(user1).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("0.5"),
        });

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user1).acceptOffer(offerParams, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds balance");
      });

      it("Should revert if not enough escrow allowance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user2.address, 1);
        await nft.connect(user2).approve(offer.address, 1);

        await escrow
          .connect(user1)
          .deposit({ value: ethers.utils.parseEther("1") });

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user1.address,
        };
        const signature = await caeateOfferSignature(
          user1,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptOffer(offerParams, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds allowance");
      });

      it("Should revert if not set escrow", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await offer.setEscrow(ethers.constants.AddressZero);
        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user1.address,
        };
        const signature = await caeateOfferSignature(
          user1,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptOffer(offerParams, signature)
        ).to.be.revertedWith("escrow is not set");
      });
    });

    describe("Transfer", function () {
      it("Should transfer NFT to bidder", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await offer.connect(user1).acceptOffer(offerParams, signature);

        expect(await nft.ownerOf(1)).to.equal(user2.address);
      });

      it("Should transfer funds to seller and market payee", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("5"),
        });

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptOffer(offerParams, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(95).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
      });

      it("Should transfer funds to seller, market payee, collection payee", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee, user3 } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const Royal = await ethers.getContractFactory("Royalty");
        const royaltyRegistry = await upgrades.deployProxy(Royal, []);
        await offer.setRoyaltyRegistry(royaltyRegistry.address);
        await royaltyRegistry.setRoyalty(nft.address, user3.address, 1000); // 10 %

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptOffer(offerParams, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("0")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(85).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          user3,
          ethers.utils.parseEther("1").mul(10).div(100)
        );
      });

      it("Should transfer funds to seller, market payee, collection payee when havce market fee register", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee, user3 } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // deploy royaltyRegistry
        const Royal = await ethers.getContractFactory("Royalty");
        const royaltyRegistry = await upgrades.deployProxy(Royal, []);
        await offer.setRoyaltyRegistry(royaltyRegistry.address);
        await royaltyRegistry.setRoyalty(nft.address, user3.address, 1000); // 10 %

        // deploy marketFeeRegistry
        const MarketFee = await ethers.getContractFactory("MarketFee");
        const feeRegistry = await upgrades.deployProxy(MarketFee, []);
        await offer.setFeeRegistry(feeRegistry.address);

        // register fee
        await feeRegistry.setFee(nft.address, 700); // 7% instead of 5%

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptOffer(offerParams, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("0")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(83).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(7).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          user3,
          ethers.utils.parseEther("1").mul(10).div(100)
        );
      });

    });

    describe("Emit event", function () {
      it("Should emit OfferAccepted event", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: OfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await caeateOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(offer.connect(user1).acceptOffer(offerParams, signature))
          .to.emit(offer, "OfferAccepted")
          .withArgs(
            nft.address,
            1,
            ethers.utils.parseEther("1"),
            user2.address,
            user1.address
          );
      });
    });
  });

  describe("Accept collection offer", function () {
    describe("Validations", function () {
      it("Should revert if not NFT owner", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.mint(user2.address, 2);

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user1).acceptCollectionOffer({
            ...offerParams,
            tokenId: 2,
          }, signature)
        ).to.be.revertedWith("not own nft");
      });

      it("Should revert if invalid signature", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          {
            ...offerParams,
            price: ethers.utils.parseEther("2"),
          },
          chainId
        );

        await expect(
          offer.connect(user1).acceptCollectionOffer({
            ...offerParams,
            tokenId: 1,
          }, signature)
        ).to.be.revertedWith("invalid signature");
      });

      it("Should revert if not approve NFT", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user1).acceptCollectionOffer({
            ...offerParams,
            tokenId: 1,
          }, signature)
        ).to.be.revertedWith("ERC721: caller is not token owner or approved");
      });

      it("Should revert if not enough escrow balance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        await escrow.connect(user1).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("0.5"),
        });

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user1).acceptCollectionOffer({
            ...offerParams,
            tokenId: 1,
          }, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds balance");
      });

      it("Should revert if not enough escrow allowance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user2.address, 1);
        await nft.connect(user2).approve(offer.address, 1);

        await escrow
          .connect(user1)
          .deposit({ value: ethers.utils.parseEther("1") });

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user1.address,
        };
        const signature = await createCollectionOfferSignature(
          user1,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptCollectionOffer({
            ...offerParams,
            tokenId: 1,
          }, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds allowance");
      });

      it("Should revert if not set escrow", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await offer.setEscrow(ethers.constants.AddressZero);
        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user1.address,
        };
        const signature = await createCollectionOfferSignature(
          user1,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptOffer({
            ...offerParams,
            tokenId: 1,
          }, signature)
        ).to.be.revertedWith("escrow is not set");
      });
    });

    describe("Transfer", function () {
      it("Should transfer NFT to bidder", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await offer.connect(user1).acceptCollectionOffer({
          ...offerParams,
          tokenId: 1,
        }, signature);

        expect(await nft.ownerOf(1)).to.equal(user2.address);
      });

      it("Should transfer funds to seller and market payee", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("5"),
        });

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptCollectionOffer({
            ...offerParams,
            tokenId: 1,
          }, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(95).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
      });

      it("Should transfer funds to seller, market payee, collection payee", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee, user3 } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const Royal = await ethers.getContractFactory("Royalty");
        const royaltyRegistry = await upgrades.deployProxy(Royal, []);
        await offer.setRoyaltyRegistry(royaltyRegistry.address);
        await royaltyRegistry.setRoyalty(nft.address, user3.address, 1000); // 10 %

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptCollectionOffer({
            ...offerParams,
            tokenId: 1,
          }, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("0")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(85).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          user3,
          ethers.utils.parseEther("1").mul(10).div(100)
        );
      });

      it("Should transfer funds to seller and market payee when have market fee register", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // deploy marketFeeRegistry
        const MarketFee = await ethers.getContractFactory("MarketFee");
        const feeRegistry = await upgrades.deployProxy(MarketFee, []);
        await offer.setFeeRegistry(feeRegistry.address);

        // register fee
        await feeRegistry.setFee(nft.address, 800);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("5"),
        });

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptCollectionOffer({
            ...offerParams,
            tokenId: 1,
          }, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(92).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(8).div(100)
        );
      });
    });

    describe("Emit event", function () {
      it("Should emit OfferAccepted event", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: CollectionOfferParams = {
          tokenAddress: nft.address,
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
        };

        // accept offer
        const signature = await createCollectionOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(offer.connect(user1).acceptCollectionOffer({
          ...offerParams,
          tokenId: 1,
        }, signature))
          .to.emit(offer, "OfferAccepted")
          .withArgs(
            nft.address,
            1,
            ethers.utils.parseEther("1"),
            user2.address,
            user1.address
          );
      });
    });
  });


  describe("Accept counter offer", function () {
    describe("Validations", function () {
      it("Should revert if not NFT owner", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.mint(user2.address, 2);

        const counterParams: CounterOfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        };
        const signature = await createOfferCounterSignature(
          user1,
          offer.address,
          counterParams,
          chainId
        );

        // transfer NFT to user2
        await nft.connect(user1).transferFrom(user1.address, user2.address, 1);

        await expect(
          offer.connect(user2).acceptCounterOffer(counterParams, signature)
        ).to.be.revertedWith("seller not own nft");
      });

      it("Should revert if not approved", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);

        const counterParams: CounterOfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        };
        const signature = await createOfferCounterSignature(
          user1,
          offer.address,
          counterParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptCounterOffer(counterParams, signature)
        ).to.be.revertedWith("ERC721: caller is not token owner or approved");
      });

      it("Should revert if not enough escrow balance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const counterParams: CounterOfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        };
        const signature = await createOfferCounterSignature(
          user1,
          offer.address,
          counterParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptCounterOffer(counterParams, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds balance");
      });

      it("Should revert if not enough escrow allowance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const counterParams: CounterOfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        };
        const signature = await createOfferCounterSignature(
          user1,
          offer.address,
          counterParams,
          chainId
        );

        await escrow.connect(user2).deposit({
          value: ethers.utils.parseEther("1"),
        });

        await expect(
          offer.connect(user2).acceptCounterOffer(counterParams, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds allowance");
      });
    });

    describe("Transfer", function () {
      it("Should transfer NFT", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const counterParams: CounterOfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        };
        const signature = await createOfferCounterSignature(
          user1,
          offer.address,
          counterParams,
          chainId
        );

        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        await offer.connect(user2).acceptCounterOffer(counterParams, signature);

        expect(await nft.ownerOf(1)).to.equal(user2.address);
      });

      it("Sould transfer funds to seller, market", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const counterParams: CounterOfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        };
        const signature = await createOfferCounterSignature(
          user1,
          offer.address,
          counterParams,
          chainId
        );

        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const acceptTx = offer.connect(user2).acceptCounterOffer(counterParams, signature);
        await acceptTx;

        expect(await escrow.balanceOf(offer.address)).to.equal(0);

        await expect(acceptTx).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(95).div(100)
        );

        await expect(acceptTx).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
      })

      it("Should transfer funds to seller, market, and royalty", async function () {
        const { offer, user1, user2, user3, nft, chainId, escrow, payee } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const Royal = await ethers.getContractFactory("Royalty");
        const royaltyRegistry = await upgrades.deployProxy(Royal, []);
        await offer.setRoyaltyRegistry(royaltyRegistry.address);
        await royaltyRegistry.setRoyalty(nft.address, user3.address, 250); // 2.5 %

        const counterParams: CounterOfferParams = {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        };
        const signature = await createOfferCounterSignature(
          user1,
          offer.address,
          counterParams,
          chainId
        );

        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const acceptTx = offer.connect(user2).acceptCounterOffer(counterParams, signature);
        await acceptTx;

        expect(await escrow.balanceOf(offer.address)).to.equal(0);

        await expect(acceptTx).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(925).div(1000)
        );
        await expect(acceptTx).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
        await expect(acceptTx).to.changeEtherBalance(
          user3,
          ethers.utils.parseEther("1").mul(25).div(1000)
        );
      })
    });
  });

  describe("Accept collection package offer", function () {
    describe("Validations", function () {
      it("Should revert if not NFT owner", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.mint(user2.address, 2);

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1, 2],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptPackageOffer(offerParams, signature)
        ).to.be.revertedWith("ERC721: caller is not token owner or approved");
      });

      it("Should revert if invalid signature", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          {
            ...offerParams,
            price: ethers.utils.parseEther("2"),
          },
          chainId
        );

        await expect(
          offer.connect(user1).acceptPackageOffer(offerParams, signature)
        ).to.be.revertedWith("invalid signature");
      });

      it("Should revert if not approve NFT", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await nft.mint(user1.address, 1);

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user1).acceptPackageOffer(offerParams, signature)
        ).to.be.revertedWith("ERC721: caller is not token owner or approved");
      });

      it("Should revert if not enough escrow balance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        await escrow.connect(user1).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("0.5"),
        });

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user1).acceptPackageOffer(offerParams, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds balance");
      });

      it("Should revert if not enough escrow allowance", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user2.address, 1);
        await nft.connect(user2).approve(offer.address, 1);

        await escrow
          .connect(user1)
          .deposit({ value: ethers.utils.parseEther("1") });

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user1.address,
          id: 123,
        };
        const signature = await createCollectionPackageOfferSignature(
          user1,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptPackageOffer(offerParams, signature)
        ).to.be.revertedWith("Escrow: transfer amount exceeds allowance");
      });

      it("Should revert if not set escrow", async function () {
        const { offer, user1, user2, nft, chainId } = await deploy();
        await offer.setEscrow(ethers.constants.AddressZero);
        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user1.address,
          id: 123,
        };
        const signature = await createCollectionPackageOfferSignature(
          user1,
          offer.address,
          offerParams,
          chainId
        );

        await expect(
          offer.connect(user2).acceptPackageOffer(offerParams, signature)
        ).to.be.revertedWith("escrow is not set");
      });
    });

    describe("Transfer", function () {
      it("Should transfer NFT to bidder", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };

        // accept offer
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await offer.connect(user1).acceptPackageOffer(offerParams, signature);

        expect(await nft.ownerOf(1)).to.equal(user2.address);
      });

      it("Should transfer funds to seller and market payee", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("5"),
        });

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };

        // accept offer
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptPackageOffer(offerParams, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("4")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(95).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
      });

      it("Should transfer funds to seller, market payee, collection payee", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee, user3 } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        const Royal = await ethers.getContractFactory("Royalty");
        const royaltyRegistry = await upgrades.deployProxy(Royal, []);
        await offer.setRoyaltyRegistry(royaltyRegistry.address);
        await royaltyRegistry.setRoyalty(nft.address, user3.address, 1000); // 10 %

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };

        // accept offer
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptPackageOffer(offerParams, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("0")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(85).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(5).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          user3,
          ethers.utils.parseEther("1").mul(10).div(100)
        );
      });

      it("Should transfer funds to seller, market payee, collection payee when market fee register was set", async function () {
        const { offer, user1, user2, nft, chainId, escrow, payee, user3 } =
          await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // deploy royaltyRegistry
        const Royal = await ethers.getContractFactory("Royalty");
        const royaltyRegistry = await upgrades.deployProxy(Royal, []);
        await offer.setRoyaltyRegistry(royaltyRegistry.address);
        await royaltyRegistry.setRoyalty(nft.address, user3.address, 1000); // 10 %

        // deploy marketFeeRegistry
        const MarketFee = await ethers.getContractFactory("MarketFee");
        const feeRegistry = await upgrades.deployProxy(MarketFee, []);
        await offer.setFeeRegistry(feeRegistry.address);

        // register fee
        await feeRegistry.setFee(nft.address, 700); // 7%

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };

        // accept offer
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        const acceptOffer = offer
          .connect(user1)
          .acceptPackageOffer(offerParams, signature);
        await acceptOffer;

        // escrow balance change
        expect(await escrow.balanceOf(user2.address)).to.equal(
          ethers.utils.parseEther("0")
        );

        // wallet balance
        await expect(acceptOffer).to.changeEtherBalance(
          user1,
          ethers.utils.parseEther("1").mul(83).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          payee,
          ethers.utils.parseEther("1").mul(7).div(100)
        );
        await expect(acceptOffer).to.changeEtherBalance(
          user3,
          ethers.utils.parseEther("1").mul(10).div(100)
        );
      });

    });

    describe("Emit event", function () {
      it("Should emit OfferAccepted event", async function () {
        const { offer, user1, user2, nft, chainId, escrow } = await deploy();
        await nft.mint(user1.address, 1);
        await nft.connect(user1).approve(offer.address, 1);

        // create offer
        await escrow.connect(user2).depositAndApprove(offer.address, {
          value: ethers.utils.parseEther("1"),
        });

        const offerParams: CollectionPackageOfferParams = {
          tokenAddress: nft.address,
          tokenIds: [1],
          price: ethers.utils.parseEther("1"),
          bidder: user2.address,
          id: 123,
        };

        // accept offer
        const signature = await createCollectionPackageOfferSignature(
          user2,
          offer.address,
          offerParams,
          chainId
        );

        await expect(offer.connect(user1).acceptPackageOffer(offerParams, signature))
          .to.emit(offer, "PackageOfferAccepted")
          .withArgs(
            nft.address,
            ethers.utils.parseEther("1"),
            user2.address,
            user1.address,
            123
          );
      });
    });
  });

});
