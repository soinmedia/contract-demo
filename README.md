# deploy accounts:
- cronos test: 0x4bB02B226A6Ac1256E6fE94a452cFA103c79e3D4
- cronos main: 0x917281fa404f777DA61b3Bba593C60329a2F010C

- polygon test: 0x4bB02B226A6Ac1256E6fE94a452cFA103c79e3D4
- polygon main: 0x917281fa404f777DA61b3Bba593C60329a2F010C

# Script
```bash
# deploy
npx hardhat run --network mumbai scripts/deploy/00-deploy-market.ts

# verify
npx hardhat verify --network mumbai 0x4bB02B226A6Ac1256E6fE94a452cFA103c79e3D4

# upgrade
npx hardhat run --network mumbai scripts/deploy/market.ts
```

## TODO:
- deploy market fee
- upgrade market
- upgrade offer
- call market fee config
