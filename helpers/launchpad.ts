import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { BigNumber } from "ethers";

export enum PaymentType {
  NATIVE = 0,
  ERC20 = 1,
  ERC721 = 2,
}

export type MintParams = {
  from: string,
  quantity: BigNumber,
  tokens: string[],
  tokenAmounts: BigNumber[],
  value: BigNumber,
};

// "MintParams(address from,uint256 amount,address[] tokens,uint256[] tokenAmounts)"
export function createMintSignature(
  wallet: SignerWithAddress,
  contractAddress: string,
  params: MintParams,
  chainId: number,
): Promise<string> {
  const domain = {
    name: "Launchpad",
    version: "1.0.0",
    chainId,
    verifyingContract: contractAddress,
  };
  const types = {
    MintParams: [
      { name: "from", type: "address" },
      { name: "quantity", type: "uint256" },
      { name: "tokens", type: "address[]" },
      { name: "tokenAmounts", type: "uint256[]" },
      { name: "value", type: "uint256" },
    ],
  };

  return wallet._signTypedData(domain, types, params);
}
