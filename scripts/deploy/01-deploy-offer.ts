import { ethers, upgrades } from "hardhat";

async function main() {
  const marketPayee = process.env.MARKET_PAYEE;
  if (!marketPayee) {
    throw new Error("MARKET_PAYEE is not defined");
  }

  const Offer = await ethers.getContractFactory("Offer");
  const offer = await upgrades.deployProxy(Offer, [marketPayee]);
  await offer.deployed();

  console.log('-----------------------------------');
  console.log('Deployed offer');
  console.log(Date());
  console.table({
    address: offer.address,
    implementation: await upgrades.erc1967.getImplementationAddress(offer.address),
  });
  console.log(`Params: ${marketPayee}`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
