import { ethers, upgrades } from "hardhat";

async function main() {
  const marketAddress = process.env.MARKET_ADDRESS;
  const offerAddress = process.env.OFFER_ADDRESS;
  const marketFeeAddress = process.env.MARKET_FEE_ADDRESS;

  if (!marketAddress || !marketFeeAddress || !offerAddress) {
    throw new Error("MARKET_ADDRESS or MARKET_FEE_ADDRESS or OFFER_ADDRESS is not defined");
  }

  const market = await ethers.getContractAt("Marketplace", marketAddress);
  const tx = await market.setFeeRegistry(marketFeeAddress);
  await tx.wait();
  console.log('setFeeRegistry market');

  const offer = await ethers.getContractAt("Offer", offerAddress);
  await offer.setFeeRegistry(marketFeeAddress);
  console.log('setFeeRegistry offer');

  console.log('-----------------------------------');
  console.log('Config registry');
  console.log(Date());
  console.table({
    market: market.address,
    offer: offer.address,
    marketFee: marketFeeAddress,
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
