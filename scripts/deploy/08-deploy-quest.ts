import { ethers, upgrades } from "hardhat";

async function main() {
  const questSigner = process.env.QUEST_SIGNER;

  if (!questSigner) {
    throw new Error("env.QUEST_SIGNER is not defined");
  }
  const Quest = await ethers.getContractFactory("Quest");
  const quest = await upgrades.deployProxy(Quest, [questSigner]);
  await quest.deployed();

  console.log('-----------------------------------');
  console.log('Deployed quest');
  console.log(Date());
  console.table({
    address: quest.address,
    implementation: await upgrades.erc1967.getImplementationAddress(quest.address),
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
