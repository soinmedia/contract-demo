import { ethers, upgrades } from "hardhat";

async function main() {
  const Escrow = await ethers.getContractFactory("Escrow");
  const escrow = await upgrades.deployProxy(Escrow, []);
  await escrow.deployed();

  console.log('-----------------------------------');
  console.log('Deployed escrow');
  console.log(Date());
  console.table({
    address: escrow.address,
    implementation: await upgrades.erc1967.getImplementationAddress(escrow.address),
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
