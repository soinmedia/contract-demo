import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "@openzeppelin/hardhat-upgrades";
import "@nomiclabs/hardhat-etherscan";
import "@cronos-labs/hardhat-cronoscan";

import { config as envConfig } from "dotenv";

envConfig();

const accounts = [process.env.PRIVATE_KEY || ""];

const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.17",
    settings: {
      optimizer: {
        enabled: true,
        runs: 400,
      },
    },
  },
  networks: {
    ganache: {
      url: "http://127.0.0.1:7545",
      accounts,
      gasPrice: "auto",
    },
    goerli: {
      url: "https://rpc.ankr.com/eth_goerli",
      accounts,
      gasPrice: "auto",
    },
    cronosTest: {
      url: "https://evm-t3.cronos.org",
      accounts,
      gasPrice: "auto",
    },
    cronos: {
      url: "https://evm.cronos.org",
      accounts,
      gasPrice: "auto",
    },
    mumbai: {
      url: "https://polygon-mumbai.blockpi.network/v1/rpc/public",
      accounts,
      gasPrice: "auto",
    },
    polygon: {
      url: "https://polygon-bor.publicnode.com",
      accounts,
      gasPrice: "auto",
    },
  },
  etherscan: {
    apiKey: process.env.API_KEY,
  },
};

export default config;
